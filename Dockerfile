FROM node:18-alpine AS build

RUN yarn global add pnpm

WORKDIR /app

COPY ./package.json ./pnpm-lock.yaml .

RUN pnpm install --frozen-lockfile --prod

FROM node:18-alpine

WORKDIR /app

COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/package.json ./package.json

COPY ./index.js ./index.js

EXPOSE 8000

CMD ["node" , "index.js"]
